#! /usr/bin/env python3

# Script to show weather forecast from OpenWeatherMap.org
#
# Usage: weather.py LOCATION
#
# Author: Radek Sprta <mail@radeksprta.eu>
# Date: 2017-06-19
# License: The MIT License

import json
import sys

import requests

API_TOKEN = 'd70d5b8400a03658504b0ec3f48843d9'


def get_location():
    """
    Gets location from commandline.

    Returns:
        str: Location for weather forecast.
    """
    if len(sys.argv) < 2:
        # Check for arguments
        print('Usage: weather.py LOCATION')
        sys.exit()
    location = ' '.join(sys.argv[1:])
    return location


def get_forecast(location, token):
    """
    Gets weather forecast from OpenWeatherMap.org

    Args:
        location(str): Location for weather forecast

    Returns:
        dict: JSON with weather forecast.
    """
    url = 'http://api.openweathermap.org/data/2.5/forecast/daily?q={}&cnt=3&APPID={}'.format(
        location, token)
    response = requests.get(url)
    response.raise_for_status()
    return response.text


def parse(data):
    """
    Parse the weather data and return printable forecast.

    Args:
        data(dict): JSON with weather forecast data.

    Return:
        list: List of forecasts for different days.
    """
    weather_data = json.loads(data)
    forecast = weather_data['list']
    output = []
    for i in range(3):
        main = forecast[i]['weather'][0]['main']
        desc = forecast[i]['weather'][0]['description']
        temp = to_celsius(forecast[i]['temp']['day'])
        output.append('{0:.1f} {1} - {2}'.format(temp, main, desc))
    return output


def to_celsius(kelvin):
    """
    Convert temperature in Kelvin to Celsius

    Args:
        kelvin(float): Temperature in Kelvin.

    Returns:
        float: Temperature in Celsius.
    """
    return kelvin - 273


def main():
    """
    Main function of the script.
    """
    location = get_location().capitalize()
    try:
        forecast = get_forecast(location, API_TOKEN)
        print('Weather forecast for {}.'.format(location))
        print('\n'.join(parse(forecast)))
    except requests.exceptions.HTTPError as e:
        print('Could not get weather forecast.')
    except requests.exceptions.ConnectionError as e:
        print('Could not connect.')
    sys.exit()


if __name__ == '__main__':
    main()
